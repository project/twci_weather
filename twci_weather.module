<?php

module_load_include('php', 'twci_weather', 'includes/twci_weather.parser.cls');

function twci_weather_system($field) {
  $system["description"] = t("Feeds off weather.com xml services and outputs to a block.");
  return $system[$field];
}

/**
 * Menu callback; displays the block configuration form.
 */
function twci_weather_admin_settings() {	

	$form = array();
	
	$form['twci_weather_settings'] = array(
	    '#type' => 'fieldset',
	    '#title' => t('TWCi Weather settings'),
	    '#collapsible' => TRUE,
	);
	
	$form['twci_weather_settings']['twci_weather_partnerid'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Partner ID'),
	    '#maxlength' => 50,
	    '#default_value' => variable_get('twci_weather_partnerid', '')	  
  	);
	
  	$form['twci_weather_settings']['twci_weather_licensekey'] = array(
	    '#type' => 'textfield',
	    '#title' => t('License key'),
	    '#maxlength' => 50,
	    '#default_value' => variable_get('twci_weather_licensekey', '')	  
  	);
		
	$units = array("m"=>"celsius", "s"=>"fahrenheit");
  	$form['twci_weather_settings']['weather_unit'] = array(
	    '#type' => 'select',
	    '#title' => t('Units'),
	    '#default_value' => variable_get('weather_unit', 'm'),
	    '#options' => $units
  	);
	
	$icons = array("31"=>"small", "61"=>"medium", "93"=>"big");
	$form['twci_weather_settings']['weather_icons'] = array(
	    '#type' => 'select',
	    '#title' => t('Icons'),
	    '#default_value' => variable_get('weather_icons', 31),
	    '#options' => $icons

  	);  
  	
	$cache = drupal_map_assoc(array(3600, 10800, 21600, 32400, 43200), 'format_interval');
	$form['twci_weather_settings']['weather_cache'] = array(
	    '#type' => 'select',
	    '#title' => t('Cache'),
	    '#default_value' => variable_get('weather_cache', 10800),
	    '#options' => $cache
  	);  

	$form['regional_settings'] = array(
	    '#type' => 'fieldset',
	    '#title' => t('Regional settings'),
	    '#collapsible' => TRUE
	);

	$form['regional_settings']['twci_weather_region1'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Regional code 1'),
	    '#maxlength' => 64,
	    '#autocomplete_path' => 'twci_weather/region/autocomplete',
	    '#default_value' => variable_get('twci_weather_region1', '')	  
  	);
  	
 	$form['regional_settings']['twci_weather_region2'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Regional code 2'),
	    '#maxlength' => 64,
	    '#autocomplete_path' => 'twci_weather/region/autocomplete',
	    '#default_value' => variable_get('twci_weather_region2', '')	   
  	);

 	$form['regional_settings']['twci_weather_region3'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Regional code 3'),
	    '#maxlength' => 64,
	    '#autocomplete_path' => 'twci_weather/region/autocomplete',
	    '#default_value' => variable_get('twci_weather_region3', '')	   
  	);
  	
  	$form['#submit'] = array('twci_weather_admin_settings_form_submit');

  return system_settings_form($form);
}

function twci_weather_admin_settings_form_submit($form, &$form_state) {
	
	$twci_weather_partnerid  = $form['twci_weather_settings']["#post"]["twci_weather_partnerid"];
  $twci_weather_licensekey = $form['twci_weather_settings']["#post"]["twci_weather_licensekey"];

  if (empty($twci_weather_partnerid) || empty($twci_weather_licensekey)) {
  		drupal_set_message(t('Please enter a valid partner ID and license key.'));
	} else {	
		system_settings_form_submit($form, $form_state);
		$output = twci_weather_display_weather();
		cache_set("twci_weatherblock", $output,  "cache", time() - variable_get('twci_weather_cache', 10800));
	}
	$form_state['redirect'] = 'admin/settings/twci_weather';
}

function twci_weather_block($op = "list", $delta = 0, $edit = array()) {
	switch($op) {
	  case 'list' :
	    $blocks[0]["info"] = t("TWCi Weather");
	    return $blocks;
	  	break;
	  	
	  case 'configure':
			$form = array();
			return $form;
	  	break;
	  	
	  default:
	    $block["subject"] = t("TWCi Weather");
	    $cache = cache_get("twci_weatherblock");
	
			// We rebuild the twci_weather block based on cache settings of the module
			if (!empty($cache) && $cache->created > time() - variable_get('twci_weather_cache', 10800)) {
					$block['content'] = $cache->data;
			} else {
					$output = twci_weather_display_weather();
					$block['content'] = $output;
					cache_set("twci_weatherblock",  $output, "cache", time() - variable_get('twci_weather_cache', 10800));
		    }
	
	    return $block;
  }
}

/**
 * Implementation of hook_menu().
 */
function twci_weather_menu() {
 
  $items= array();
	
	$items['admin/settings/twci_weather'] = array(
    'title'             => t('TWCi Weather'),
	  'description'		=> t('Administration of the weather menu'),
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('twci_weather_admin_settings'),
    'access callback'   => 'user_access',
    'access arguments'  => array('administer site configuration'),
    'type'              => MENU_NORMAL_ITEM
	);

	$items['twci_weather/region/autocomplete'] = array(	    
    'title' => t('Auto completion for twci_weather/region'),
    'page callback' => 'twci_weather_region_autocomplete',
   	'access arguments'  => array('access content'),
    'type' => MENU_CALLBACK
 	);
   
	return $items;
}

/* autocomplete handler */
function twci_weather_region_autocomplete($string = '') {
  
	$data = array();
  
	if (!empty($string)) {
		$string = substr($_GET['q'], strlen('twci_weather/region/autocomplete/'));
		module_load_include('php', 'twci_weather', 'includes/TWCiParser.class');
		$weather =& new TWCiParser();
		try {				
			$data = $weather->search($string, 20);
		} catch (Exception $e) {
			$data[] = array(0=>"Unable to find result");
		}		
	}
  	
	print drupal_to_js($data);
	exit();  	
}

function twci_weather_javascript_pngfix() {
  $content  = "<!--[if gte IE 5.5000]>";
  $content .= "<script type=\"text/javascript\" src=\"modules/twci_weather/pngfix.js\"></script>";
  $content .= "<![endif]-->";
  return $content;
}

function twci_weather_display_weather(){

  // check user agent and generate png fix for IE if needed
  $usrAgent = $_SERVER["HTTP_USER_AGENT"];
  if ( strpos($usrAgent, "MSIE") === false) {
     $content = "";
  } 
  else {
     $content  = twci_weather_javascript_pngfix();
	}

  // parse xml and create content
  $xweather = new twci_weather_parser();
  $content    = "";
  
  $xweather->set_partnerid (variable_get('twci_weather_partnerid', '')); 
  $xweather->set_licensekey (variable_get('twci_weather_licensekey', '')); 
  
  for($i = 1 ; $i <=3 ; $i++) {
  	$content .= $xweather->build_content_block(variable_get("twci_weather_region" . $i, ""));
  }
  $content .= $xweather->build_content_footer();
 
  return $content;
}

function twci_weather_display_forecast() {
}

function twci_weather_display_feed() {
}

function twci_weather_xml_icon($url) {
  global $base_url;
  $this_site = preg_replace("/http:\/\//", "", $base_url);
  $url = variable_get("clean_url", 0) ? $url : '?q=$url';
  return "<p><div align=\"center\" class=\"xml-icon\"><a href=\"$url\"><img src=\"module/twci_weather/xml.gif\" width=\"36\" height=\"14\" alt=\"XML feed provided by weatherroom.com and locally hosted by " . $this_site ."\" /></a></div>";
}

?>