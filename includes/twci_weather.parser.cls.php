<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class weather_parser
//
// Allows to retrieve and extract the weather data
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class twci_weather_parser {

  var $tag = "";
  var $region="";

  var $xml_parser;
  var $print_feed=false;
  var $moonTagOpened=false;

  var $partnerid  = "";
  var $licensekey = "";

  var $feed;
  var $weather = array();
  
  var $error;

  function weather_parser($region = "") {
	if ($this->region != "")
  		$this->region = $region;
  }

  function build_content_footer() {
	  return "<div align=\"center\"><br/><a href=\"http://www.weather.com/?prod=xoap&par=" . $this->partnerid ."\"><img src=\"http://" . $_SERVER["SERVER_NAME"] . base_path() . drupal_get_path('module', 'twci_weather') . "/images/TWClogo_31px.png\" /></a></div>";
  }

  function buildImgUrl($param = "") {
    return "<a href=\"http://www.weather.com/?prod=xoap&par=" . $this->partnerid . "\"><img src=\"http://" . $_SERVER["SERVER_NAME"] . base_path() . drupal_get_path('module', 'twci_weather') . "/images/" . variable_get('weather_icons', 31) . "/" . $param . ".png\" /></a><br/>";
  }

  function build_content_block($region) {

    if ($region == "")
    	return;

    $this->region = $region;
    $weather_input = "";
  
    // url to access the webservice using my keys
  
    $url = "http://xoap.weather.com/weather/local/" . $this->region . "?cc=*&prod=xoap&link=xoap&unit=" . variable_get('weather_unit', 'm') . "&par=" . $this->partnerid ."&key=" . $this->licensekey;
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    $weather_input=curl_exec ($ch);
    curl_close ($ch);
  
    if ($print_feed) {
  	  return $this->feed = $weather_input;
    }
  
    // create the parser and parse out the file
  
    $xml_parser = @xml_parser_create();
    $this->xml_parser = $xml_parser;
    xml_set_object($this->xml_parser, $this);
    xml_set_element_handler($this->xml_parser, "start_element", "end_element");
    xml_set_character_data_handler($this->xml_parser, "character_data");
    xml_parse($this->xml_parser, $weather_input);
    xml_parser_free($this->xml_parser);
  
    
    if (isset($this->weather['0']['ERR']) && !empty($this->weather['0']['ERR'])) {
      $this->error = $this->weather['0']['ERR'];
      return false;
    }
    
    $content  = "<div><br/></div>\n";
    $content .= "<div><strong>" . $this->weather[0]['DNAM'] . "</strong></div>\n";
  
    if ( $this->weather[0]['ICON'] != "-" ) {
    	$content .= "<div>" . $this->buildImgUrl ( $this->weather[0]['ICON'] ) . "</div>\n";
    } else {
    	$content .= "<div>" . $this->buildImgUrl ( "na" ) . "</div>\n";
    }
    $content .= "<div>" . $this->weather[0]['TMP'] . " &ordm;" . $this->weather[0]['UT'] . " " . $this->weather[0]['T'] . "</div>\n";
    $content .= "<div>Sunrise : " . $this->weather[0]['SUNR'] . "</div>\n";
    $content .= "<div>Sunset  : " . $this->weather[0]['SUNS'] . "</div>\n";
  
    return $content;

  }

  function set_region($region) {
    $this->region = $region;
  }

  function get_partnerid() {
  	return $partnerid;
  }

  function set_partnerid($partnerid) {
    $this->partnerid = $partnerid;
  }

  function get_licensekey() {
  	return $licensekey;
  }

  function set_licensekey($licensekey) {
    $this->licensekey = $licensekey;
  }

  // load tag
  function start_element($parser, $tagName, $attrs) {
    $this->tag = $tagName;
    if ($tagName == "MOON") {
    	$this->moonTagOpened = true;
    }
  }

  // free tag
  function end_element($parser, $tagName) {
    $this->tag = "";
    if ($tagName == "MOON") {
    	$this->moonTagOpened = false;
    }
  }

  // process & store text data for our weather class
  function character_data($parser, $data) {

    if ($this->tag == "")
    	return;

  	$data = trim($data);

  	if ($this->moonTagOpened) {
  	  $this->weather[0]['MOON'.$this->tag] = $data;
  	} 
  	else $this->weather[0][$this->tag] = $data;
  
    }
}
?>