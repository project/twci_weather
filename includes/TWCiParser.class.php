<?php

/**
 * Project:     RSSParser: A library for parsing RSS feeds
 * File:        RSSParser.class.php
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @link http://www.phpinsider.com/php/code/RSSParser/
 * @copyright 2004-2005 New Digital Group, Inc.
 * @author Monte Ohrt <monte at newdigitalgroup dot com>
 * @package RSSParser
 * @version 1.0-dev
 */

require_once(dirname(__FILE__).'/XMLParser.class.php');

class TWCiParser extends XMLParser{
    
    /**#@-*/
    /**
     * The class constructor.
     */
    function TWCiParser() { }
  
    /**
     * parse the XML file (or URL)
     *
     * @param string $path the XML file path, or URL
     */
    function parse($data){

        // parse the XML content      
        parent::parse($data);

        if(empty($this->output))
            return array();
            
        // detect the format        
        switch(strtoupper($this->output[0]['name'])) {
            case 'SEARCH':
                return $this->parse_search();
                break;
            default:
                trigger_error('unknown XML format');
                break;
        }
    }

    function parse_search() {
        // get RSS header items
        $_output['ATTRS'] = $this->output[0]['attrs'];
        if (!is_array($this->output[0]['child'])) return array();
        
        // get channel content
        foreach($this->output[0]['child'] as $_channel_element) {
            switch(strtoupper($_channel_element['name'])) {
                case 'LOC':
                    $_output['LOC'][$_channel_element['attrs']['ID']] = $_channel_element['content'];
                    break;
                /*default:
                    $_output[$_channel_element['name']] = isset($_channel_element['content']) ? $_channel_element['content'] : null;
                    break;   */
            }   

        }
        return $_output;
    }
    
    function search($q) {	  
  	  $url = "http://xoap.weather.com/search/search?where=" . urlencode($q);
  	  
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
      $weather_input=curl_exec ($ch);
      curl_close ($ch);
      
      $output = $this->parse($weather_input);
      //debug($weather_input);
      if (empty($output['LOC'])) return array('' => 'Unable to find anything');
      else return $output['LOC'];
  	}  
}


?>
